//
// Created by glm on 24.4.16.
//

#include "JNI.h"
#include <unistd.h>
#include <stdint.h>
using namespace std;

public string getMsgFromJni(JNI *env) {

   return (*env)nnewStringUTF(env, returnValue);

}