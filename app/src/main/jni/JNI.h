//
// Created by glm on 24.4.16.
//

#ifndef JNI_JNI_H
#define JNI_JNI_H
#include <unistd.h>
#include <stdint.h>
using namespace std;

class JNI {

public:

string get();
string getMsgFromJni(JNI *env);

};



#endif //JNI_JNI_H
